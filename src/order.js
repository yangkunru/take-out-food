import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    // Need to be implement
    this._itemsDetails = dishes.filter((product) => itemsMap.has(product.id)).map(element => {
      return {
        id: element.id,
        name: element.name,
        price: element.price,
        count: itemsMap.get(element.id)
      };
    });
  }

  get itemsDetails () {
    // Need to be implement
    return this._itemsDetails;
  }

  get totalPrice () {
    // Need to be implement
    return this.itemsDetails.map((element) => element.price * element.count).reduce((result, current) => {
      return result + current;
    }, 0);
  }
}

export default Order;

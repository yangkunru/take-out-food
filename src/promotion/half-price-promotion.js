import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type () {
    return this._type;
  }

  includedHalfPriceDishes () {
    // need to be completed
    return this.order.itemsDetails.filter((element) => this.halfPriceDishes.includes(element.id));
  }

  discount () {
    // need to be completed
    return this.includedHalfPriceDishes().map((element) => element.price * element.count / 2).reduce((accumulative, current) => accumulative + current, 0);
  }
}

export default HalfPricePromotion;

function parseInputToMap(selectedItems) {
  // need to be implement
  const menuMap = new Map();
  selectedItems.split(',').forEach(element => {
    const lineArray = element.split(' x ');
    menuMap.set(lineArray[0], parseInt(lineArray[1]));
  });
  return menuMap;
}

export { parseInputToMap };
